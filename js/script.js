document.addEventListener('click', function(event){
	if(event.target.classList.contains('burger')){
		document.querySelector('.burger-button').classList.toggle('burger-button-active');
		document.querySelector('.nav-bar').classList.toggle('nav-bar-active');
	}	
});